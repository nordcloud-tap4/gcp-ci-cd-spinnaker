variable "project" {
  description = "The ID of the project to apply any resources to."
  default = "gcp-ci-cd-poc"
}

variable "zone" {
  default = "europe-north1-a"
}

variable "gcs_location" {
  default = "europe-north1"
}

variable "service_account" {
  default = "terraform"
}

variable "cluster_name" {
  default     = "spinnaker"
  description = "GKE cluster name"
}

variable "cluster_nodes_count" {
  default     = 3
  description = "Number of nodes in the GKE cluster"
}

variable "node_type" {
  default     = "n1-standard-2"
  description = "VM Node type"
}

variable "spinnaker_version" {
  default = "1.11.0"
  description = "Spinnaker Version (hal version list)"
}
